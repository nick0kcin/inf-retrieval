import pandas as pd
import subprocess
from config import repo_list, path_to_clone


def clone_url(url):
    try:
        res = subprocess.check_output(['cd {0} && git clone --depth 1 {1}'.format(path_to_clone, url)], shell=True)
    except:
        pass
    return 0


if __name__ == '__main__':
    df = pd.read_csv(repo_list)
    df['clone_url'].apply(clone_url)
