import requests
import pandas as pd
import os.path as osp


def urljoin(*args):
    return '/'.join(path.strip('/') for path in args)


class Crawler:
    BASE = 'https://api.github.com'
    USER_AGENT = 'Crawler/Python'

    def __init__(self, user=None, password=None):
        self.u = user
        self.pwd = password
        if self.u is not None and self.pwd is None:
            self.pwd = input('Enter password for user {}:'.format(self.u))

    def _get(self, path, **kwargs):
        params = kwargs.pop('params', {})
        if self.u is not None:
            r = requests.get(urljoin(self.BASE, path), headers=dict(user_agent=self.USER_AGENT),
                             auth=(self.u, self.pwd), params=params)
        else:
            r = requests.get(urljoin(self.BASE, path), headers=dict(user_agent=self.USER_AGENT), params=params)
        print('Querying {}'.format(r.url))
        return r

    def get_repos(self, query, *, n_pages=1, from_page=1):
        page = from_page
        result = []
        for i in range(n_pages):
            params = dict(q=query, sort='stars', order='asc', per_page=100, page=page)
            r = self._get('/search/repositories', params=params)
            result.append(pd.DataFrame(r.json()['items']))
            page += 1

        return pd.concat(result)
