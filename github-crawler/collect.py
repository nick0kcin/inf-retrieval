from crawler import Crawler
from config import query, repo_list
import os.path as osp


if __name__ == '__main__':
    gh = Crawler()
    r = gh.get_repos(query, n_pages=5)
    r.to_csv(repo_list, header=True, encoding='utf-8')