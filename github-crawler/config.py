import os.path as osp

# uncomment to gather 1st dataset
# query = 'language:python3 size:<1000 forks:0 NOT django NOT template NOT lectures NOT lab NOT learn pushed:>2018-01-01'

# uncomment to gather 2nd dataset
query = 'language:python3 size:<1000 sort:stars NOT django NOT template NOT lectures NOT lab NOT learn pushed:>2018-01-01'
# user = <github_username>

repo_list = osp.join('../data', 'repos.csv')
path_to_clone = '../data'
