package spark

import scala.util.matching.Regex

case object PathConcat extends FeatureType
{
  override def extract(data: Iterable[String]):Double={
    val slashConc= data.filter(line=>(new Regex("\\+\\s*(['\"])\\/\\1") findFirstIn line).nonEmpty )
    val joinConc= data.filter(line=>(new Regex("path\\.join") findFirstIn line).nonEmpty )

    if (joinConc.isEmpty && slashConc.isEmpty)
      return -1
    val slashCount = if (slashConc.nonEmpty) slashConc.map(s=>1.longValue()).reduce(_+_).toDouble else 0.0
    val joinCount = if (joinConc.nonEmpty) joinConc.map(s=>1.longValue()).reduce(_+_).toDouble else 0.0
    return  joinCount/(slashCount+joinCount)
  }
}
