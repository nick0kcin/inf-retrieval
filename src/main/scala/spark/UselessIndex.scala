package spark

import scala.collection.mutable
import scala.util.matching.Regex

case object UselessIndex extends FeatureType {

  def countIndent(line: String): Int = {
    if (line.isEmpty || line.trim.startsWith("#"))
      return Int.MaxValue
    val regex = "(^\\s*)\\S".r
    val searchRes=regex.findFirstIn(line)
    return if (searchRes.isEmpty) Int.MaxValue else searchRes.get.length-1
  }

  override def extract(data: Iterable[String]): Double = {
    //variable_name,indent,body
    var stack: mutable.MutableList[(String, Int, String)] = mutable.MutableList()
    //variable_name,body
    var cycles: mutable.MutableList[(String, String)] = mutable.MutableList()

    data.foreach(line => {
      val indent = countIndent(line.replace("#", " "))
      val trimmed = line.replace("#", " ").trim

      //pop stack
      while (!stack.isEmpty && stack.last._2 >= indent) {
        val el = (stack.last._1, stack.last._3)
        cycles += el
        stack = stack.dropRight(1)
      }
      if (!stack.isEmpty) {
        stack = stack.map(entry => (entry._1, entry._2, entry._3+line+"\n"))
      }
      //push new cycle if needed
      val pattern = "for\\s*(\\w+)\\sin.*:".r
      trimmed match {
        case pattern(varName) => {
          stack.+=((varName, indent, ""))
        }
        case _ =>
      }
    })
    //clean stack
    while (!stack.isEmpty) {
      val el = (stack.last._1, stack.last._3)
      cycles += el
      stack = stack.dropRight(1)
    }
    //compute feature
    val findRes = cycles.filter(pair =>
      s"\\w+\\s*\\[.*\\b${pair._1}\\b.*\\]".r.findAllIn(pair._2).length == s"\\b${pair._1}\\b".r.findAllIn(pair._2).length)
    if (cycles.isEmpty)
      return -1
    if (findRes.isEmpty)
     return 1
    return 1-findRes.map(_ => 1.longValue()).reduce(_ + _).toDouble / cycles.map(_ => 1.longValue()).reduce(_ + _)
  }
}
