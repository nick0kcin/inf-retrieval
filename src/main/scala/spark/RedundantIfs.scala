package spark

import scala.util.matching.Regex

// 1- ratio of if (expression) return True/False (that could be just return) to all ifs
case object RedundantIfs extends FeatureType {
  override def extract(data: Iterable[String]): Double = {
    val ifRegex = new Regex("\\bif\\b.+:")
    val ifSimpleRegex = new Regex("\\bif\\b.+:\\n\\s+return\\s+(?:True|False)")
    val newData = data.mkString("\n")
    val totalIfCount = ifRegex.findAllIn(newData).size.toFloat
    if (totalIfCount == 0)
      return -1
    val badIfCount = ifSimpleRegex.findAllIn(newData).size
    return (totalIfCount - badIfCount) / totalIfCount
  }
}
