package spark

import scala.util.matching.Regex

case object PrintConcat extends FeatureType
{
  override def extract(data: Iterable[String]):Double={
    val allConc= data.filter(line=>(new Regex("print\\s*\\(.*(\\+|,).*\\)") findFirstIn line).nonEmpty )
    val goodConc=allConc.filter(line=>(new Regex("print\\s*\\(.*,.*\\)") findFirstIn line).nonEmpty )

    if(allConc.isEmpty)
      return -1
    if (goodConc.isEmpty)
      return 0
    val allConcCount=allConc.map(s=>1.longValue()).reduce(_+_)
    return   goodConc.map(s=>1.longValue()).reduce(_+_).toDouble/allConcCount
  }
}
