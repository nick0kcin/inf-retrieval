package spark

import scala.util.matching.Regex

// 1 - ratio of ifs and whiles that have len(...) > 0 or != 0 as one of their conditions ratio to all ifs & whiles
case object LenInBoolContext extends FeatureType {
  override def extract(data: Iterable[String]): Double = {
    val conditionalRegex = new Regex("\\b(?:if|while)\\b\\s*([^:]+)\\s*:")
    val lenRegex = new Regex("len\\(.+\\)\\s+(?:>|!=)\\s+0")
    val conditionals = data.flatMap(line => conditionalRegex.findAllMatchIn(line).map(m => m.group(1)))
    val totalCondCount = conditionals.size.toFloat
    if (totalCondCount == 0)
      return -1
    val goodCondCount = conditionals.count(line => lenRegex.findFirstIn(line).isEmpty)
    return goodCondCount / totalCondCount
  }
}
