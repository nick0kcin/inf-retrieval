package spark

import scala.util.matching.Regex


// ratio of good style for python's method definition respect to all python methods in code
case object CamelCase extends FeatureType {
  override def extract(data: Iterable[String]): Double = {
    val allFunc = data.filter(line => (new Regex("\\bdef\\b") findFirstIn line).nonEmpty)
    val goodFunc = allFunc.filter(line => (new Regex("\\bdef\\s+[a-z_]+\\b") findFirstIn line).nonEmpty)
    if (allFunc.isEmpty)
      return -1
    if (goodFunc.isEmpty)
      return 0

    val allFuncCount = allFunc.map(s => 1.longValue()).reduce(_ + _)
    return goodFunc.map(s => 1.longValue()).reduce(_ + _).toDouble / allFuncCount
  }
}

