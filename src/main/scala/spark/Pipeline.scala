package spark

import org.apache.spark.sql.SparkSession

import scala.io.Source

object Main {
  val root = "data/"

  def main(args: Array[String]) {
    val session = SparkSession
      .builder().master("local")
      .getOrCreate()

    val reposTable = session.read.option("header", "true").csv(root + "repos.csv")

    val nameIndex = reposTable.first.fieldIndex("full_name")

    import session.implicits._

    //разработчик, список файлов
    val shortInfo = reposTable.map(s => s.getString(nameIndex)).map(s => s.split("/")).flatMap(s =>
      FileListMaker.make(root + s(1)).map(el => (s(0), el))).rdd

    val featureExtractor = new Features(Array(CamelCase, NoneCond, PrintConcat, ListComp, PathConcat, RedundantIfs,
      LenInBoolContext, UselessIndex))

    val dataset = shortInfo.flatMap(el => Source.fromFile(el._2).getLines().map(s => (el._1, s)))
      .groupByKey()
      .map(el => (el._1, featureExtractor.extract(el._2).mkString(",")))

    dataset.saveAsTextFile("Dataset")

  }
}
