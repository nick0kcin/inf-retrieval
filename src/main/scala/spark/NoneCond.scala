package spark

import scala.util.matching.Regex

case object NoneCond extends FeatureType
{
  override def extract(data: Iterable[String]):Double={
    val allCond= data.filter(line=>(new Regex("(\\sis\\s|==)\\s*None") findFirstIn line).nonEmpty )
    val goodCond=allCond.filter(line=>(new Regex("\\sis\\s+None") findFirstIn line).nonEmpty )

    if (allCond.isEmpty)
      return -1
    if (goodCond.isEmpty)
      return 0
    val allCondCount=allCond.map(s=>1.longValue()).reduce(_+_)
    return   goodCond.map(s=>1.longValue()).reduce(_+_).toDouble/allCondCount
  }
}
