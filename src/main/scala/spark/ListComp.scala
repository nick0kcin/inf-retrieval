package spark

import scala.util.matching.Regex

case object ListComp extends FeatureType {
  override def extract(data: Iterable[String]): Double = {
    val dummyList = data.filter(line => (new Regex("\\[\\s*([-0-9.]+|(['\"]).*\\2)\\s+for") findFirstIn line).nonEmpty)
    val trueList = data.filter(line => (new Regex("\\[\\s*([-0-9.]+|((['\"]).*\\3))\\s*\\]\\s*\\*") findFirstIn line).nonEmpty)

    if (trueList.isEmpty && dummyList.isEmpty)
      return -1
    if (trueList.isEmpty)
      return 0.0

    val trueCount = trueList.map(s => 1.longValue()).reduce(_ + _).toDouble
    val dummyCount = if (dummyList.nonEmpty) dummyList.map(s => 1.longValue()).reduce(_ + _).toDouble else 0.0
    return trueCount / (trueCount + dummyCount)
  }
}
