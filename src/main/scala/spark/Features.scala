package spark

trait FeatureType {
  def extract(data: Iterable[String]): Double
}


case class Features(featureTypes: Array[FeatureType]) {

  def extract(data: Iterable[String]): Array[Double] = {
    featureTypes.map(B => B.extract(data))
  }
}
