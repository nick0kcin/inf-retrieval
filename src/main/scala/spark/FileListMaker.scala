package spark

import java.io._

object FileListMaker {

  def make(dir: File): Array[String] = {

    dir.listFiles() match {
      case null => Array()
      case files => files.filter(f=> f.getName().endsWith(".py") &&  f.length>0).map(_.getAbsolutePath()) ++ files.filter(_.isDirectory).flatMap(s => make(s))
    }
  }

  def make(dir: String): Array[String] = {
    return make(new File(dir))
  }

  def makeString(dir: String, sep: String): String = {
    return make(dir).mkString(sep)
  }

}
