from os import path as osp
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('ggplot')


def plot(f):
    def decorated(*args, **kwargs):
        plt.figure()
        f(*args, **kwargs)
        # plt.show()
    return decorated

def make_autopct(values):
    """
    Credits to https://stackoverflow.com/questions/6170246/how-do-i-use-matplotlib-autopct
    """
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
    return my_autopct


@plot
def plot_pie(column):
    column[(column > 0) & (column < 1)] = .5
    column.value_counts().rename({0: 'Bad', .5: 'Both', 1: 'Good', -1: 'Not found'}).sort_index().plot.pie(autopct='%.2f%%')
    plt.title(column.name)
    plt.ylabel('# repos')
    plt.savefig(osp.join('plots', 'pie_{}_{}.png'.format(column.name.replace(' ', '_'), SUFFIX)))


@plot
def plot_pie_closer(column):
    column[(column > 0) & (column < 1)] = .5
    column = column[column >= 0]
    column.value_counts().rename({0: 'Bad', .5: 'Both', 1: 'Good', -1: 'Not found'}).sort_index().plot.pie(autopct='%.2f%%')
    plt.title(column.name)
    plt.ylabel('# repos')
    plt.savefig(osp.join('plots', 'pie_{}_closer_{}.png'.format(column.name.replace(' ', '_'), SUFFIX)))

@plot
def plot_users_count(df):
    # Number of users with the feature present
    df = df.copy()
    df.columns = FEATURES_SHORT
    ((df >=0) & (df < 1)).sum(axis=0).plot.bar()
    plt.title('Number of users for each feature')
    plt.ylabel('Users')
    plt.savefig(osp.join('plots', 'users_for_feature_{}.png'.format(SUFFIX)))

@plot
def plot_features_count(df):
    # Number of users with the feature present
    vc = ((df >=0) & (df < 1)).sum(axis=1).value_counts()
    vc['total'] = len(df)
    print(vc)
    vc.plot.bar()#.hist(bins=len(FEATURES))
    plt.title('How many features do they have?')
    plt.ylabel('Users')
    plt.xlabel('#features')
    plt.savefig(osp.join('plots', 'users_for_nfeatures_{}.png'.format(SUFFIX)))


SUFFIX = 'good'
PATH = osp.join('Dataset_{}'.format(SUFFIX), 'part-00000')
FEATURES_SHORT = ['f{}'.format(i) for i in range(1,9)]
FEATURES = ['Camel case', 'Using == None', 'Using + in print()', 'Constant in comprehention', 'Path Concatenation', 'Redundant Ifs', 'Using len() in ifs',
             'Useless index']


if __name__ == '__main__':
    with open(PATH) as f:
        lines = [line.lstrip('(').rstrip(')\n').split(',') for line in f]

    df = pd.DataFrame(lines, columns=['user']+FEATURES)
    df = df.set_index('user')
    df[FEATURES] = df[FEATURES].apply(pd.to_numeric, axis=0)
    df[FEATURES].apply(plot_pie, axis=0)
    df[FEATURES].apply(plot_pie_closer, axis=0)

    plot_users_count(df)
    plot_features_count(df)
